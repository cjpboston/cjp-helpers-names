#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_namespace_packages

# with open('README.rst') as readme_file:
#     readme = readme_file.read()

requirements = []

setup(
    author="cjp",
    author_email="danylok@cjp.org",
    classifiers=[
        "Development Status :: 3 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.8",
    ],
    description="A project to contain CJP's of the occasionally useful helper functions.",
    install_requires=requirements,
    license="BSD license",
    include_package_data=True,
    name="cjp_helpers_names",
    packages=find_namespace_packages(),
    url="https://gitlab.com/cjpboston/cjp-helpers-names",
    version="0.2.1",
    zip_safe=False,
)
