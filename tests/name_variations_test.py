import pytest
from cjp.name_prep import name_variations



def test_extract_names_raises_type_error_expects_str():
    with pytest.raises(TypeError):
        name_variations.extract_name_parts(22)


def test_extract_names_raises_value_error_expects_alpha():
    with pytest.raises(ValueError):
        name_variations.extract_name_parts(" - ---  -- ")


@pytest.mark.parametrize(
    "name, expected_variations",
    [
        (" bOb - ", {("BOB", 1, 1)}),
        (
            "A-b ",
            {("A", 1, 3), ("A B", 2, 3), ("A-B", 3, 3), ("AB", 2, 3), ("B", 1, 3)},
        ),
        (
            "A-b  c",
            {
                ("A", 1, 5),
                ("A B", 2, 5),
                ("A B C", 4, 5),
                ("A-B", 3, 5),
                ("A-B C", 5, 5),
                ("AB", 2, 5),
                ("AB C", 4, 5),
                ("ABC", 3, 5),
                ("B", 1, 5),
                ("C", 1, 5),
            },
        ),
    ],
)
def test_extract_names(name, expected_variations):
    assert name_variations.extract_name_parts(name) == expected_variations

