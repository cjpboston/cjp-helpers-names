import pathlib
import csv
import itertools
import re
from typing import Set, Tuple


def prep_name_part(part: str) -> str:
    # confirm the name field is a string
    if not isinstance(part, str):
        raise TypeError("The part variable passed is not a String")
    # strip all punctuation except hyphen
    part = re.sub(r"[^\w|\-|\s]", "", part)
    part = part.replace("\n", " ")
    part = part.strip(" - \t")
    part = part.upper()
    # consolidate conscutive hyphens and/or spaces
    part = re.sub(r"\s*?-+\s*-*\s*", "-", part)
    # consolidate leftover consecutive spaces
    part = re.sub(r"\s+", " ", part)
    # at this point, name constains the prepped name as the master value
    return part


def extract_name_parts(name: str) -> Set[Tuple[str, int, int]]:
    """ takes a string and returns a list of parsed names, delimitting based on space or hyphen characters """
    # spaces and hyphens are the only delimters
    # each segment is counted as a unit of value 1
    # spaces and hyphens count as segments
    # consecutive delimiters are reduced to single delimiters, with a preference for hyphens
    # i.e. "Smith - Cohen  Westerson" becomes "Smith-Cohen Westerson" with five segments
    # special case: a variation where hyphens get mapped to spaces, but don't count as a segment.
    # special case: segments delimted by hyphens only get parsed internally - parts don't map to others.
    # i.e. "Smith-Cohen Westerson" has variations: "Smith-Cohen", "Smith", "Cohen", but not "Cohen Westerson"
    # the variation score is the number of segments used (special case hyphen-substitution excluded)
    # divided by number of segments in original reduced string.

    name = prep_name_part(name)

    if len(name) == 0:
        raise ValueError("The stripped string is empty")

    # split name into "name segments"
    name_segments = name.split()
    num_hyphen = name.count("-")
    num_segments = (len(name_segments) * 2 - 1) + (num_hyphen * 2)
    # redefine name_segments to be a list of lists where the inner list is items that were separated by hyphens
    name_segments = [seg.split("-") for seg in name_segments]
    variations = set()
    # special case:
    no_spaces = "".join(itertools.chain(*name_segments))
    variations.add((no_spaces, num_segments - num_hyphen - name.count(" ")))

    # build rest of components
    components = []  # created ordered list of weighted component sets
    for segment in name_segments:
        # each item in name_segments is a list where there's only one item unless there had been a hyphen
        # was there a hyphen?
        if len(segment) > 1:
            variations.update(
                [(s, 1) for s in segment]
            )  # special handling for sub-parts of hyphenated names.
            space_sub = (" ".join(segment), len(segment))
            hyphen_sub = ("-".join(segment), len(segment) * 2 - 1)
            none_sub = ("".join(segment), len(segment))
            components.append({space_sub, hyphen_sub, none_sub})
        else:
            components.append({(segment[0], 1)})
    for width in range(1, len(components) + 1):
        for i in range(0, len(components) - width + 1):  # look at slices of len(width)
            combos = itertools.product(
                *components[i : i + width]
            )  # create cartesian product of variations
            for combo in combos:  # parse each combo.
                name_parts = [part[0] for part in combo]
                name_score = sum([part[1] for part in combo]) + len(combo) - 1
                variation = (" ".join(name_parts), name_score)
                variations.add(variation)
    variations = {(v[0], v[1], num_segments) for v in variations}
    return variations


def load_nick_dict():
    """ reads nicknames csv returning a dictionary of sets 
    key is the diminutive and the set is the list of given names """
    nick_dict = dict()
    current_dir = pathlib.Path(__file__).parent
    nick_path = current_dir.joinpath("data/nick_to_name.csv")
    with open(nick_path) as nick_csv:  # load csv into dict
        nick_reader = csv.reader(nick_csv)
        for nick_row in nick_reader:
            given_names = set(nick_row[1:])
            given_names.add("")
            given_names.remove("")
            nick_dict[nick_row[0]] = given_names
    return nick_dict


def lookup_full_first(dimunitive: str) -> Set[str]:
    """ lookup dimunitive in the nick_to_name.csv file and return the corresponding proper first names on that row """
    try:
        full_firsts = nick_dict[dimunitive]
        full_firsts = full_firsts.copy()
    except KeyError:
        full_firsts = set()
    return full_firsts


def build_name_variations(
    first_name: str,
    last_name: str,
    middle_name: str = "",
    maiden_name: str = "",
    nick_name: str = "",
):
    """ build list of dictionaries with all the name variations generated from input name parts
    each dictionary has first name, score, middle name, score, last name, score.
    Score is supposed to represent a fraction where first value (numerataor) is number of name parts used and 
    second value (denominator) is total possible name parts each name type like first, middle, last """
    # create name part variations for first and last name
    first_parts = extract_name_parts(first_name)
    last_parts = extract_name_parts(last_name)

    # setup the middle names
    # prep case & remove detritus
    middle_name = prep_name_part(middle_name)
    # setup the end result of the huge summit discussing the handling middle name
    # no middle name is worth zero
    if len(middle_name) == 0:
        middle_parts = {("", 0, 0)}
    # initial only is worth 1  and also create a blank option
    elif len(middle_name) == 1:
        middle_parts = {(middle_name, 1, 1), ("", 0, 1)}
    # here we got a whole name so it's worth 2 and setup the options for initial and blank
    else:
        middle_parts = {(middle_name, 2, 2), (middle_name[0], 1, 2), ("", 0, 2)}
    # clean up maiden_name
    maiden_name = prep_name_part(maiden_name)
    # as long as there is a maiden_name...
    if maiden_name:
        # save a single item in the set in order to retrieve the score denominator
        last_part = last_parts.pop()
        # put it back into the main set
        last_parts.add(last_part)
        # add maiden name to last names with appropriate score denominator
        last_parts.add((maiden_name, 1, last_part[2] + 1))

    # all part of building the alternate firsts (nick_name lookups)
    nick_name = prep_name_part(nick_name)
    first_name = prep_name_part(first_name)
    alternate_firsts = lookup_full_first(first_name)
    # add the passed nickname value to alternate firsts
    if nick_name:
        alternate_firsts.add(nick_name)
    # add alternate firsts to the set of first names with appropriate score denominator as calculated in maiden
    if alternate_firsts:
        first_part = first_parts.pop()
        first_parts.add(first_part)
        for alt in alternate_firsts:
            first_parts.add((alt, 1, first_part[2] + 1))
    # empty list of name variations
    name_variations = []
    # create catesian product for the first, middle, last name parts
    # iterate through them setting up dictionary items for easy use w/pandas
    for f, m, l in itertools.product(first_parts, middle_parts, last_parts):
        name_variation = {}
        name_variation["first_name"] = f[0]
        name_variation["first_score"] = [f[1], f[2]]
        name_variation["middle_name"] = m[0]
        name_variation["middle_score"] = [m[1], m[2]]
        name_variation["last_name"] = l[0]
        name_variation["last_score"] = [l[1], l[2]]
        # append to name variations list of dictionaries
        name_variations.append(name_variation)
    return name_variations


# this is the nickname lookup list as read from the CSV
nick_dict = load_nick_dict()

# test code
# p = build_name_variations("dan", "smith", "joy")

