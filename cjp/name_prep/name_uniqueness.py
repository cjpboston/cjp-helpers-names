import pathlib
from collections import defaultdict
from typing import Dict
from pickle import load as pkl_load

# from memory_profiler import profile

# global scope placeholder for name list to lookup uniqueness
first_uniscores = None
last_uniscores = None


# @profile
def firstname_uniqueness(first_name: str) -> Dict[str, float]:
    """Looks up first name in us census data, returns a dictionary with "female_count", "male_count", "total_count", and "per100k" values.

    Args:
        first_name (str): first name to look up in census data. alpha only

    Returns:
        Dict[str,int]: returns {"per100k":float, "total_count":float, "male_count":float, "female_count":float}
    """
    # for first names, the default values are a little more complicated.
    # if we have 307M counted and the US population is 328M, then we're missing 21M people, about 6.4%
    # the low threshold to be counted in our list is 5 names and there are 15,000 names with only 5 people.
    # any other name we might try is probabyly
    # set as defaultdict w/default values: females=45, males=45, total=90, per100k=0.029316
    # first = defaultdict(lambda: (45.0, 45.0, 90.0, 0.029316), first)
    # the above line works, but it can't be pickled because of the lambda function
    # one solution is to use a normal function to return the tuple as default value
    # but this doesn't actually store the defaults - only the function call
    # so wherever we load the pkl, we need this dv function available to provide the values
    # first = defaultdict(dv, first)
    # def dv():
    #     return (2.5, 2.5, 5, 0.001627)

    first_name = "".join([c for c in first_name if c.isalpha()])  # strips non-alpha
    first_name = first_name.upper()  # upper cases
    global first_uniscores
    if not first_uniscores:
        # first_uniscores = pkl_load(open("data/first.pkl", "rb"))
        current_dir = pathlib.Path(__file__).parent
        first_pkl_path = current_dir.joinpath("data/first.pkl")
        with open(first_pkl_path, "rb") as fp:
            first_uniscores = pkl_load(fp)
    try:
        first_unis = {
            "female_count": first_uniscores[first_name][0],
            "male_count": first_uniscores[first_name][1],
            "total_count": first_uniscores[first_name][2],
            "per100k": first_uniscores[first_name][3],
        }
    except KeyError:
        first_unis = {
            "female_count": 2.5,
            "male_count": 2.5,
            "total_count": 5,
            "per100k": 0.001627,
        }
    return first_unis


# @profile
def lastname_uniqueness(last_name: str) -> Dict[str, float]:
    """Looks up last name in us census data, returns a dictionary with "per100k" and "total_count" values.

    Args:
        last_name (str): last name to look up in census data. alpha only

    Returns:
        Dict[str,int]: returns {"per100k":float, "total_count":float}
    """
    # define interior function to return default values for defaultdict
    # the grand total count for last names is a little lower than for first names, so
    # the default count is 87 instead of 90 (to get per100k < a value that rounds to 0.03)
    # def dv():
    #     return (87.0, 0.029494)
    # last = defaultdict(dv, last)
    last_name = "".join([c for c in last_name if c.isalpha()])  # strips non-alpha
    last_name = last_name.upper()
    global last_uniscores
    if not last_uniscores:
        current_dir = pathlib.Path(__file__).parent
        last_pkl_path = current_dir.joinpath("data/last.pkl")
        # last_uniscores = pkl_load(open("data/last.pkl", "rb"))
        with open(last_pkl_path, "rb") as fp:
            last_uniscores = pkl_load(fp)
    try:
        last_unis = {
            "count": last_uniscores[last_name][0],
            "per100k": last_uniscores[last_name][1],
        }
    except KeyError:
        last_unis = {
            "count": 87.0,
            "per100k": 0.029494,
        }
    return last_unis
