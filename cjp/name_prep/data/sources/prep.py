import os
import pandas as pd
import pickle
from collections import defaultdict


def build_first():

    # the data files come from here: http://www.ssa.gov/oact/babynames/names.zip
    # for our initial run, we've included 1930 - 2019 (excluding years before 1930)
    # to avoid skewing our data by counting people who are mostly deceased.
    # when the script is run, this is the path to the folder where the data files live
    folder_name = "first"

    # empty dataframe with the columns we want
    df_all = pd.DataFrame(
        [["sample", "F", 1, 1930]], columns=["name", "gender", "count", "year"]
    )
    df_all = df_all.drop([0])

    # for each file in the folder
    for f in os.listdir(folder_name):
        if f[:3] != "yob":
            continue
        # year of birth will be the filename's characters 4 thorugh 8
        yob = f[3:7]
        # read the file into data frame
        df_current = pd.read_csv(folder_name + "/" + f, header=None)
        # fill in the year column
        df_current["year"] = yob
        # provide named headers so the append will work
        df_renamed = df_current.rename(columns={0: "name", 1: "gender", 2: "count"})
        # tack it on to the combined dataframe
        df_all = df_all.append(df_renamed, ignore_index=True)

    # pivot to sum all years into columns by gender
    df_pt = pd.pivot_table(
        df_all, values="count", index="name", columns="gender", aggfunc="sum"
    )
    # create the total column (adding female and male counts)
    df_pt["total"] = df_pt["F"].fillna(0) + df_pt["M"].fillna(0)

    # get grand total for all names, approx 307,000,000
    gt = df_pt["total"].sum()
    # create per100k on total - like perCENT only this is per 100k
    df_pt["per100k"] = 100000 * df_pt["total"] / gt

    # make name column uppercase
    # df_pt['name'] = df_pt['name'].str.upper()     # this fails b/c 'name' is the index
    # make new index, turning name into a normal column
    df_pt.reset_index(inplace=True)
    # now we can apply uppercase
    df_pt["name"] = df_pt["name"].str.upper()
    # turn name back into the index so it will become a dictionary key in the next line
    df_pt.set_index("name", inplace=True)

    df_new_first_adj = pd.read_csv(folder_name + "/" +"firstname_mvn_per100k.csv")
    df_new_first_adj.set_index("firstname", inplace=True)
    df_joined = df_pt.join(df_new_first_adj,how='outer')

    rows = df_joined.adjusted_mvn_per100k.isna()==False

    df_joined.loc[rows,['per100k']] = df_joined.loc[rows,['adjusted_mvn_per100k']]['adjusted_mvn_per100k']

    df_joined = df_joined.drop(columns=['adjusted_mvn_per100k'])

    # doing this: https://stackoverflow.com/questions/38133332/pandas-dataframe-to-dict-with-values-as-tuples
    # create a dictionary with name as the key and a tuple with the contents of F, M and total columns as the value
    #              name    females males total  per100k     name  females males total per100k
    #               ↓         ↓      ↓     ↓       ↓         ↓        ↓    ↓     ↓      ↓
    # for example 'LUCY' : (127692, 279, 127971, 41.645),  'ZZYZX': (nan, 10.0, 10.0, 0.003)
    first = df_joined.apply(tuple, 1).to_dict()
    # testing
    # first['LUCY']
    # first['Lucy']

    # write that out to pkl
    # note, this saves here in the name_uniqueness_sources folder, not in:
    #       //...//matchtool/gcf/build_name_variations/data/
    # after creating an updated pkl file, you need to copy it over to the data folder
    f = open("first.pkl", "wb")
    pickle.dump(first, f, -1)
    f.close()

    # testing
    # first_uniscores = pickle.load(open("first.pkl", "rb"))
    # first_uniscores["LUCY"]
    # first_uniscores["ZYVION"]
    # first_uniscores["Lucy"]


def build_last():
    # last names are easier
    df_current = pd.read_csv("last/Names_2010Census.csv")
    # rename this column
    df_current["per100k"] = df_current["prop100k"]
    # delete unwanted columns
    df_current = df_current.drop(
        columns=[
            "rank",
            "cum_prop100k",
            "pctwhite",
            "pctblack",
            "pctapi",
            "pctaian",
            "pct2prace",
            "pcthispanic",
            "prop100k",
        ],
        axis=1,
    )
    # include comments about what the values are
    
    # set name column as index for join
    df_current.set_index("name", inplace=True)
    # adjust per100k with Maven last name per100k and outer join to increase total count from 162,254 to 169,192
    df_new_last_adj = pd.read_csv("last/lastname_mvn_per100k.csv")
    # set lastname column of Maven csv as index
    df_new_last_adj.set_index("lastname", inplace=True)
    df_joined = df_current.join(df_new_last_adj,how='outer')
    
    rows = df_joined.adjusted_mvn_per100k.isna()==False
    # replace the per100k values with adjusted_mvn_per100k
    df_joined.loc[rows,['per100k']] = df_joined.loc[rows,['adjusted_mvn_per100k']]['adjusted_mvn_per100k']
    # delete the adjusted_mvn_per100k column
    df_joined = df_joined.drop(columns = ['adjusted_mvn_per100k'])
    
    # do the same thing  as above to get the dict w/tuples for values
    last = df_joined.apply(tuple, 1).to_dict()

    # testing
    # last['VANPELT']
    # last['VanPelt']

    # write out to pkl
    # note, this saves here in the name_uniqueness_sources folder, not in:
    #       //...//matchtool/gcf/build_name_variations/data/
    # after creating an updated pkl file, you need to copy it over to the data folder
    f = open("last.pkl", "wb")
    pickle.dump(last, f, -1)
    f.close()
    # testing
    # last_uniscores = pickle.load(open("last.pkl", "rb"))
    # last_uniscores['VANPELT']
    # last_uniscores['VanPelt']
